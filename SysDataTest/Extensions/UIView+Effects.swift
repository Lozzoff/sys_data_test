//
//  UIView+Effects.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

extension UIView {
    
    func addBorder(radius: Int, color: UIColor, width: Int) {
        layer.cornerRadius = CGFloat(radius)
        layer.borderWidth = CGFloat(width)
        layer.borderColor = color.cgColor
        clipsToBounds = true
    }
}
