//
//  BaseNavigationController.swift
//  Teleport
//
//  Created by Siarhei on 15/12/2018.
//  Copyright © 2018 Stork-TGK. All rights reserved.
//

import Foundation
import UIKit
import SwipeTransition

class BaseNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBack = SwipeBackController(navigationController: self)
    }
}
