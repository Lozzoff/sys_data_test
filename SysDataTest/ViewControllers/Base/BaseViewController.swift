//
//  BaseViewController.swift
//  Teleport
//
//  Created by Siarhei on 15/12/2018.
//  Copyright © 2018 Stork-TGK. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    func applicationManager() -> ApplicationManager {
        return ApplicationManager.sharedInstance()
    }
    
    func showBottomAlert(text: String?) {
        let alertView = BottomAlertView.create()
        alertView.titleLabel.text = text
        alertView.show(controller:self)
    }
}
