//
//  DetailsViewController.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit
import SDWebImage
import MessageUI

class DetailsViewController: BaseViewController {

    var user = User()
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var address1Label: UILabel!
    @IBOutlet weak var address2Label: UILabel!
    @IBOutlet weak var address3Label: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var otherDetailsLabel: UILabel!
    
    class func controller() -> DetailsViewController {
        return UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    func configureUI() {
        if let url = URL(string: user.avatar) {
            avatarImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "default_image"))
        } else {
            avatarImageView.image = UIImage(named: "default_image")
        }
        
        nameLabel.text = user.name + user.surname
        address1Label.text = user.address1
        address2Label.text = user.address2
        address3Label.text = user.address3
        emailButton.setTitle("SEND EMAIL TO " + user.email, for: .normal)
        otherDetailsLabel.text = user.other
    }
    
    @IBAction func emailTapped(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([user.email])
            mail.setMessageBody("<p>SysDataTest Email</p>", isHTML: true)
            present(mail, animated: true)
        } else {
            self.showBottomAlert(text: "You can't send Emails")
        }
    }
    
    @IBAction func phone1Tapped(_ sender: Any) {
        if let number = URL(string: "tel://" + user.phone1) {
            UIApplication.shared.open(number)
        } else {
            self.showBottomAlert(text: "Sorry! Phone number is empty")
        }
    }
    
    @IBAction func phone2Tapped(_ sender: Any) {
        if let number = URL(string: "tel://" + user.phone2) {
            UIApplication.shared.open(number)
        } else {
            self.showBottomAlert(text: "Sorry! Phone number is empty")
        }
    }
    
    
}

extension DetailsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
