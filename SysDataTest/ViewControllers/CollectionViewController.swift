//
//  CollectionViewController.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit
import MBProgressHUD

class CollectionViewController: BaseViewController {

    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentCollectionView.register(UserCollectionViewCell.cellNib(), forCellWithReuseIdentifier: UserCollectionViewCell.cellIdentifier()!)
        loadUsers()
    }
    
    func loadUsers() {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        applicationManager().loadUsers(success: { result in
            hud.hide(animated: true)
            if let users = result as? [User] {
                self.users = users
                self.contentCollectionView.reloadData()
            }
        }) { error in
            hud.hide(animated: true)
            self.showBottomAlert(text: error?.localizedDescription)
        }
    }
}

extension CollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return UserCollectionViewCell.itemSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = contentCollectionView.dequeueReusableCell(withReuseIdentifier: UserCollectionViewCell.cellIdentifier()!, for: indexPath) as! UserCollectionViewCell
        cell.configure(user: users[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = DetailsViewController.controller()
        controller.user = users[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
