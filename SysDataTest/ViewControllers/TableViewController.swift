//
//  TableViewController.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit
import MBProgressHUD
import SVPullToRefresh

class TableViewController: BaseViewController {

    @IBOutlet weak var contentTable: UITableView!
    
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentTable.register(UserTableViewCell.cellNib(), forCellReuseIdentifier: UserTableViewCell.cellIdentifier()!)
        contentTable.addPullToRefresh {
            self.loadUsers()
        }
        loadUsers()
    }
    
    func loadUsers() {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        applicationManager().loadUsers(success: { result in
            hud.hide(animated: true)
            self.contentTable.pullToRefreshView.stopAnimating()
            if let users = result as? [User] {
                self.users = users
                self.contentTable.reloadData()
            }
        }) { error in
            hud.hide(animated: true)
            self.contentTable.pullToRefreshView.stopAnimating()
            self.showBottomAlert(text: error?.localizedDescription)
        }
    }
}

extension TableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UserTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contentTable.dequeueReusableCell(withIdentifier: UserTableViewCell.cellIdentifier()!) as! UserTableViewCell
        cell.configure(user: users[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DetailsViewController.controller()
        controller.user = users[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
    }
}
