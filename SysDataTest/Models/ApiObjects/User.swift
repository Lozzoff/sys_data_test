//
//  User.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

class User {
    
    var name = ""
    var surname = ""
    var thumbnail = ""
    var avatar = ""
    var address1 = ""
    var address2 = ""
    var address3 = ""
    var phone1 = ""
    var phone2 = ""
    var email = ""
    var other = ""
    
    
    class func user(dict: [String : Any]) -> User {
        let user = User()
        if let nameDict = dict["name"] as? [String : Any] {
            if let first = nameDict["first"] as? String {
                user.name = first
            }
            if let last = nameDict["last"] as? String {
                user.surname = last
            }
        }
        if let locationDict = dict["location"] as? [String : Any] {
            if let city = locationDict["city"] as? String {
                user.address1 = city
            }
            if let state = locationDict["state"] as? String {
                user.address2 = state
            }
            if let street = locationDict["street"] as? String {
                user.address3 = street
            }
        }
        if let email = dict["email"] as? String {
            user.email = email
        }

        if let phone1 = dict["cell"] as? String {
            user.phone1 = phone1
        }
        
        if let phone2 = dict["phone"] as? String {
            user.phone2 = phone2
        }
        
        if let pictureDict = dict["picture"] as? [String : Any] {
            if let thumbnail = pictureDict["thumbnail"] as? String {
                user.thumbnail = thumbnail
            }
            if let avatar = pictureDict["large"] as? String {
                user.avatar = avatar
            }
        }
        if let gender = dict["gender"] as? String {
            user.other = user.other + " Gender:" + gender
        }
        if let dobDict = dict["dob"] as? [String : Any] {
            if let age = dobDict["age"] as? String {
                user.other = user.other + "\n Age:" + age
            }
        }
        return user
    }
    
}
