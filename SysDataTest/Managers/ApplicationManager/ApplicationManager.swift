//
//  ApplicationManager.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

class ApplicationManager {

    let apiModel = SysDataApiModel()
    
    class func sharedInstance() -> ApplicationManager {
        return (AppDelegate.sharedApplication()!._applicationManager)!
    }
    
    func logout() {
    }
}
