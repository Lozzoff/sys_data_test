//
//  ApplicationManager+ApiCalls.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

extension ApplicationManager {
    
    func loadUsers(success: @escaping completionBlock, failure: @escaping failureBlock) {
        apiModel.loadUsers(success: { object in
            var users = [User]()
            if let result = object as? [String : Any] {
                if let results = result["results"] as? [[String : Any]] {
                    for dict in results {
                        users.append(User.user(dict: dict))
                    }
                }
                success(users)
            } else {
                failure(nil)
            }
        }, failure: failure)
    }
}
