//
//  UserCollectionViewCell.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

private let kUserCollectionViewCellIdentifier = "UserCollectionViewCell"

class UserCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.addBorder(radius: 0, color: UIColor.gray, width: 1)
    }
    
    override class func cellNib() -> UINib? {
        return UINib(nibName: kUserCollectionViewCellIdentifier, bundle: Bundle.main)
    }
    
    override class func cellIdentifier() -> String? {
        return kUserCollectionViewCellIdentifier
    }
    
    override class func itemSize() -> CGSize {
        let size = UIScreen.main.bounds.size.width / 2
        return CGSize(width: size, height: size)
    }

    func configure(user: User) {
        if let url = URL(string: user.avatar) {
            avatarImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "default_image"))
        } else {
            avatarImageView.image = UIImage(named: "default_image")
        }
        nameLabel.text = user.name + user.surname
        adressLabel.text = user.address1
        phoneLabel.text = user.phone1
    }
}
