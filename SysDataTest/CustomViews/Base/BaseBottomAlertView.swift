//
//  BaseBottomAlertView.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

class BaseBottomAlertView: UIView {

    var timeOfDisappearance = 2.0
    var currentController = UIViewController()
    
    func alertViewRect() -> CGRect {
        let yPos = UIScreen.main.bounds.size.height - alertViewHeight()
        return CGRect(x: 0, y: yPos, width: UIScreen.main.bounds.size.width, height: alertViewHeight())
    }
    
    func hiddenAlertViewRect() -> CGRect {
        return CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: alertViewHeight())
    }
    
    func show(controller : UIViewController) {
        currentController = controller
        let view = controller.view
        view!.addSubview(self)
        self.frame = hiddenAlertViewRect()
        UIView.animate(withDuration: 0.5, animations: {
            self.frame = self.alertViewRect()
        }) { result in
            Timer.scheduledTimer(timeInterval: self.timeOfDisappearance, target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
        }
    }

    
    @objc func hide() {
        UIView.animate(withDuration: 0.5, animations: {
            self.frame = self.hiddenAlertViewRect()
        }, completion: { result in
            self.removeFromSuperview()
        })
    }
    
    func alertViewHeight() -> CGFloat {
        return 60
    }
}
