//
//  BaseTableViewCell.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func reuseIdentifier() -> String? {
        return BaseTableViewCell.cellIdentifier()
    }
    
    class func cellIdentifier() -> String? {
        return nil
    }
    
    class func cellNib() -> UINib? {
        return UINib(nibName: String(describing: type (of:self)), bundle: Bundle.main)
    }
    
    class func cellHeight() -> CGFloat {
        return 50.0
    }

}
