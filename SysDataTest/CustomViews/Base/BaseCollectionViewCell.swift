//
//  BaseCollectionViewCell.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    var indexPath: IndexPath?
    
    func reuseIdentifier() -> String? {
        return BaseCollectionViewCell.cellIdentifier()
    }
    
    class func cellIdentifier() -> String? {
        return nil
    }
    
    class func cellNib() -> UINib? {
        return UINib(nibName: String(describing: type (of:self)), bundle: Bundle.main)
    }
    
    class func itemSize() -> CGSize {
        return CGSize(width: 0, height: 0)
    }
}
