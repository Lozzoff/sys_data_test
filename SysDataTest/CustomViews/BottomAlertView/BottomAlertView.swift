//
//  BottomAlertView.swift
//  Teleport
//
//  Created by Alexander Lozovoy on 18/01/2019.
//  Copyright © 2019 Stork-TGK. All rights reserved.
//

import UIKit

class BottomAlertView: BaseBottomAlertView {
    
    @IBOutlet weak var titleLabel: UILabel!

    class func create() -> BottomAlertView {
        return Bundle.main.loadNibNamed("BottomAlertView", owner: nil, options: nil)?.last as! BottomAlertView
    }
}
