//
//  UserTableViewCell.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit
import SDWebImage

private let kUserTableViewCellIdentifier = "UserTableViewCell"

class UserTableViewCell: BaseTableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.addBorder(radius: 0, color: UIColor.gray, width: 1)
    }
    
    override class func cellNib() -> UINib? {
        return UINib(nibName: kUserTableViewCellIdentifier, bundle: Bundle.main)
    }
    
    override class func cellIdentifier() -> String? {
        return kUserTableViewCellIdentifier
    }
    
    override class func cellHeight() -> CGFloat {
        return 60.0
    }
    
    func configure(user: User) {
        if let url = URL(string: user.avatar) {
            avatarImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "default_image"))
        } else {
            avatarImageView.image = UIImage(named: "default_image")
        }
        nameLabel.text = user.name + user.surname
        adressLabel.text = user.address1
        phoneLabel.text = user.phone1
    }
}
