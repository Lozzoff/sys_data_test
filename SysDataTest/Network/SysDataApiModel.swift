//
//  SysDataApiModel.swift
//  SysDataTest
//
//  Created by Alexander Lozovoy on 16/05/2019.
//  Copyright © 2019 Alexander Lozovoy. All rights reserved.
//

import UIKit
import Alamofire

typealias completionBlock = (Any?) -> Void
typealias failureBlock = (Error?) -> Void

class SysDataApiModel {

    func loadUsers(success: @escaping completionBlock, failure: @escaping failureBlock) {
        Alamofire.request(URL(string: "https://randomuser.me/api/?results=20")!).responseJSON { response in
            guard response.result.isSuccess else {
                failure(response.error)
                return
            }
            success(response.value)
        }
    }
}
